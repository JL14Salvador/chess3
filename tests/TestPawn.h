#ifndef TESTPAWN_H
#define TESTPAWN_H

#include <cppunit/extensions/HelperMacros.h>
#include "../Piece.h"
#include "../ChessBoard.h"
#include "../Movement.h"

//using namespace CppUnit; 


class TestPawn : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE( TestPawn ); 
	CPPUNIT_TEST( testConstructor ); 
	CPPUNIT_TEST( testLeft ); 
	CPPUNIT_TEST( testRight ); 
	CPPUNIT_TEST( testUp); 
	CPPUNIT_TEST( testDown ); 
	CPPUNIT_TEST( testUpLeft ); 
	CPPUNIT_TEST( testUpRight ); 
	CPPUNIT_TEST( testDownLeft ); 
	CPPUNIT_TEST( testDownRight ); 
	CPPUNIT_TEST( testIsAlive ); 
	CPPUNIT_TEST( testKill ); 
	CPPUNIT_TEST( testColour ); 
	CPPUNIT_TEST_SUITE_END(); 
	ChessBoard* b; 
	Piece* pW;
	Piece* pB; 

public: 
	void setUp(); 
	void tearDown(); 	
	void testConstructor(); 
	void testLeft(); 
	void testRight(); 
	void testUp();
	void testDown();
	void testUpLeft();
	void testUpRight();
	void testDownLeft();
	void testDownRight();
	void testJump();  
	void testIsAlive();
	void testKill(); 
	void testColour(); 	
};

#endif
