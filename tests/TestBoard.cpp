#include "TestBoard.h"
#include "../Exceptions.h"
#include "../KingMovement.h"

//Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION( TestBoard );

void TestBoard::setUp() {
	b = new Board(3,3); 
	p1 = new Piece(Piece::white, 'K', new KingMovement); 
	 
}

void TestBoard::tearDown() {
	delete b; 
	delete p1; 
}

void TestBoard::testConstructor() {
	CPPUNIT_ASSERT_EQUAL( 3, b->height );
	CPPUNIT_ASSERT_EQUAL( 3, b->width ); 
}


void TestBoard::testPlacePiece() {
	b->placePiece(p1, b->getSquare(0,0));
	CPPUNIT_ASSERT(b->getSquare(0,0)->getPiece() == p1); 
}

void TestBoard::testMovePiece() {
	b->placePiece(p1, b->getSquare(0,0));

	//Check to see if a piece is moved to the new square 
	b->movePiece(b->getSquare(0,0), b->getSquare(1,1)); 	
	CPPUNIT_ASSERT(b->getSquare(1,1)->getPiece() == p1); 

	//Test to see if the square the piece was moved from is empty 
	CPPUNIT_ASSERT(b->getSquare(0,0)->getPiece() == NULL);

	b->getSquare(1,1)->removePiece(); 
}

void TestBoard::testBounds() {
	CPPUNIT_ASSERT_THROW(b->placePiece(p1, b->getSquare(4,4)), invalid_coordinates_error);
	b->placePiece(p1, b->getSquare(1,1));
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(1,1), b->getSquare(5,5)), invalid_coordinates_error); 
	b->getSquare(1,1)->removePiece(); 
}

void TestBoard::testGetSquare() {
	
}