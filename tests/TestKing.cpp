#include "TestKing.h" 
#include "../KingMovement.h"
#include "../Exceptions.h"

//Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION( TestKing );

void TestKing::setUp() {
	b = new ChessBoard(3,3); 
	k = new Piece(Piece::white, 'k', new KingMovement); 	
}

void TestKing::tearDown() {
	delete b; 
	delete k; 
}

void TestKing::testConstructor() {
	CPPUNIT_ASSERT_EQUAL(Piece::white, k->player);
	CPPUNIT_ASSERT_EQUAL('k', k->symbol);
}

void TestKing::testLeft() {
	//Kings can move left 
	b->placePiece(k, b->getSquare(1,1));
	b->movePiece(b->getSquare(1,1), b->getSquare(1,0)); 
	CPPUNIT_ASSERT(b->getSquare(1,0)->getPiece() == k); 
	b->getSquare(1,0)->removePiece(); 
}

void TestKing::testRight() {
	//Kings can move right 
	b->placePiece(k, b->getSquare(1,1));
	b->movePiece(b->getSquare(1,1), b->getSquare(1,2)); 
	CPPUNIT_ASSERT(b->getSquare(1,2)->getPiece() == k); 
	b->getSquare(1,2)->removePiece(); 
}

void TestKing::testUp() {
	//Kings can move up 
	b->placePiece(k, b->getSquare(1,1));
	b->movePiece(b->getSquare(1,1), b->getSquare(0,1)); 
	CPPUNIT_ASSERT(b->getSquare(0,1)->getPiece() == k); 
	b->getSquare(0,1)->removePiece(); 
}

void TestKing::testDown() {
	//Kings can move down
	b->placePiece(k, b->getSquare(1,1));
	b->movePiece(b->getSquare(1,1), b->getSquare(2,1)); 
	CPPUNIT_ASSERT(b->getSquare(2,1)->getPiece() == k); 
	b->getSquare(2,1)->removePiece(); 
}

void TestKing::testUpLeft() {
	//Kings can move up left 
	b->placePiece(k, b->getSquare(1,1));
	b->movePiece(b->getSquare(1,1), b->getSquare(0,0)); 
	CPPUNIT_ASSERT(b->getSquare(0,0)->getPiece() == k); 
	b->getSquare(0,0)->removePiece(); 
}

void TestKing::testUpRight() {
	//Kings can move up right
	b->placePiece(k, b->getSquare(1,1));
	b->movePiece(b->getSquare(1,1), b->getSquare(0,2)); 
	CPPUNIT_ASSERT(b->getSquare(0,2)->getPiece() == k); 
	b->getSquare(0,2)->removePiece(); 
}

void TestKing::testDownLeft() {
	//Kings can move down left 
	b->placePiece(k, b->getSquare(1,1));
	b->movePiece(b->getSquare(1,1), b->getSquare(2,0)); 
	CPPUNIT_ASSERT(b->getSquare(2,0)->getPiece() == k); 
	b->getSquare(2,0)->removePiece(); 
}

void TestKing::testDownRight() {
	//Kings can move down right 
	b->placePiece(k, b->getSquare(1,1));
	b->movePiece(b->getSquare(1,1), b->getSquare(2,2)); 
	CPPUNIT_ASSERT(b->getSquare(2,2)->getPiece() == k); 
	b->getSquare(2,2)->removePiece(); 
}

void TestKing::testTwoSpaces() {
	b->placePiece(k, b->getSquare(0,0));
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(0,0), b->getSquare(2,2)), invalid_move_error);
	b->getSquare(0,0)->removePiece(); 
}

void TestKing::testJump() {

}

void TestKing::testIsAlive() {
	CPPUNIT_ASSERT_EQUAL(true, k->isAlive());
}

void TestKing::testKill() {
	k->kill(); 
	CPPUNIT_ASSERT_EQUAL(false, k->isAlive());
}

void TestKing::testColour() {
	///Tested by constructor
}