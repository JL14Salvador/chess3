#include "TestQueen.h"
#include "../QueenMovement.h" 
#include "../Exceptions.h"

//Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION( TestQueen );

void TestQueen::setUp() {
	b = new ChessBoard(5,5); 
	q = new Piece(Piece::white, 'q', new QueenMovement);

}

void TestQueen::tearDown() {
	delete b; 
	delete q; 
}

void TestQueen::testConstructor() {
	CPPUNIT_ASSERT_EQUAL(Piece::white, q->player);
	CPPUNIT_ASSERT_EQUAL('q', q->symbol);
}

void TestQueen::testLeft() {
	//Queens can move left
	b->placePiece(q, b->getSquare(2,2));
	b->movePiece(b->getSquare(2,2), b->getSquare(2,0)); 
	CPPUNIT_ASSERT(b->getSquare(2,0)->getPiece() == q); 
	b->getSquare(2,0)->removePiece(); 
}

void TestQueen::testRight() {
	//Queens can move right 
	b->placePiece(q, b->getSquare(2,2));
	b->movePiece(b->getSquare(2,2), b->getSquare(2,4)); 
	CPPUNIT_ASSERT(b->getSquare(2,4)->getPiece() == q); 
	b->getSquare(2,4)->removePiece(); 
}

void TestQueen::testUp() {
	//Queens can move up 
	b->placePiece(q, b->getSquare(2,2));
	b->movePiece(b->getSquare(2,2), b->getSquare(0,2)); 
	CPPUNIT_ASSERT(b->getSquare(0,2)->getPiece() == q); 
	b->getSquare(0,2)->removePiece(); 
}

void TestQueen::testDown() {
	//Queens can move down 
	b->placePiece(q, b->getSquare(2,2));
	b->movePiece(b->getSquare(2,2), b->getSquare(4,2)); 
	CPPUNIT_ASSERT(b->getSquare(4,2)->getPiece() == q); 
	b->getSquare(4,2)->removePiece(); 

}

void TestQueen::testUpLeft() {
	//Queens can move up left 
	b->placePiece(q, b->getSquare(2,2));
	b->movePiece(b->getSquare(2,2), b->getSquare(0,0)); 
	CPPUNIT_ASSERT(b->getSquare(0,0)->getPiece() == q); 
	b->getSquare(0,0)->removePiece(); 
}

void TestQueen::testUpRight() {
	//Queens can move up right 
	b->placePiece(q, b->getSquare(2,2));
	b->movePiece(b->getSquare(2,2), b->getSquare(0,4)); 
	CPPUNIT_ASSERT(b->getSquare(0,4)->getPiece() == q); 
	b->getSquare(0,4)->removePiece(); 
}

void TestQueen::testDownLeft() {
	//Queens can move down left 
	b->placePiece(q, b->getSquare(2,2));
	b->movePiece(b->getSquare(2,2), b->getSquare(4,0)); 
	CPPUNIT_ASSERT(b->getSquare(4,0)->getPiece() == q); 
	b->getSquare(4,0)->removePiece(); 
}

void TestQueen::testDownRight() {
	//Queens can move down right 
	b->placePiece(q, b->getSquare(2,2));
	b->movePiece(b->getSquare(2,2), b->getSquare(4,4)); 
	CPPUNIT_ASSERT(b->getSquare(4,4)->getPiece() == q); 
	b->getSquare(4,4)->removePiece(); 
}

void TestQueen::testJump() {

}

void TestQueen::testIsAlive() {
	CPPUNIT_ASSERT_EQUAL(true, q->isAlive());
}

void TestQueen::testKill() {
	q->kill(); 
	CPPUNIT_ASSERT_EQUAL(false, q->isAlive());
}

void TestQueen::testColour() {
	///Tested by constructor
}