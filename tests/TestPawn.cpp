#include "TestPawn.h" 
#include "../PawnMovement.h"
#include "../Exceptions.h"
#include "../Game.h"

//Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION( TestPawn );


void TestPawn::setUp() {
	b = new ChessBoard(3,3); 
	pW = new Piece(Piece::white, 'p', new PawnMovement);
	pB = new Piece(Piece::black, 'P', new PawnMovement); 

}

void TestPawn::tearDown() {
	delete b; 
	delete pW; 
	delete pB;
}

void TestPawn::testConstructor() {
	CPPUNIT_ASSERT_EQUAL(Piece::white, pW->player);
	CPPUNIT_ASSERT_EQUAL('p', pW->symbol);
	CPPUNIT_ASSERT_EQUAL(Piece::black, pB->player);
	CPPUNIT_ASSERT_EQUAL('P', pB->symbol);
}


void TestPawn::testLeft() {
	//White pawns can't move left
	b->placePiece(pW, b->getSquare(1,1));
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(1,1), b->getSquare(1,0)), invalid_move_error);
	b->getSquare(1,1)->removePiece(); 

	//Black pawns can't move left
	b->placePiece(pB, b->getSquare(1,1));
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(1,1), b->getSquare(1,0)), invalid_move_error);
	b->getSquare(1,1)->removePiece(); 
}

void TestPawn::testRight() {
	//White pawns can't move right
	b->placePiece(pW, b->getSquare(1,1));
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(1,1), b->getSquare(1,2)), invalid_move_error);
	b->getSquare(1,1)->removePiece(); 

	//Black pawns can't move right 
	b->placePiece(pB, b->getSquare(1,1));
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(1,1), b->getSquare(1,2)), invalid_move_error);
	b->getSquare(1,1)->removePiece(); 
}

void TestPawn::testUp() {
	//White pawns can't move up
	b->placePiece(pW, b->getSquare(1,1));
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(1,1), b->getSquare(0,1)), invalid_move_error);
	b->getSquare(1,1)->removePiece(); 

	//Change turn because white always starts
	Game::turn++; 	

	//Black pawns can move up	
	b->placePiece(pB, b->getSquare(1,1));
	b->movePiece(b->getSquare(1,1), b->getSquare(0,1));
	CPPUNIT_ASSERT(b->getSquare(0,1)->getPiece() == pB);
	b->getSquare(0,1)->removePiece(); 

	//Change turn so that white starts again
	Game::turn++; 	
}

void TestPawn::testDown() {
	//White pawns can move down
	b->placePiece(pW, b->getSquare(1,1));
	b->movePiece(b->getSquare(1,1), b->getSquare(2,1));
	CPPUNIT_ASSERT(b->getSquare(2,1)->getPiece() == pW);
	b->getSquare(2,1)->removePiece(); 

	//Black pawns can't move down
	b->placePiece(pB, b->getSquare(1,1));
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(1,1), b->getSquare(2,1)), invalid_move_error);
	b->getSquare(1,1)->removePiece(); 
}

void TestPawn::testUpLeft() {
	//White pawns can't move diagonally
	b->placePiece(pW, b->getSquare(1,1));
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(1,1), b->getSquare(0,0)), invalid_move_error);
	b->getSquare(1,1)->removePiece(); 

	//Black pawns can't move diagonally
	b->placePiece(pB, b->getSquare(1,1));
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(1,1), b->getSquare(0,0)), invalid_move_error);
	b->getSquare(1,1)->removePiece(); 
}

void TestPawn::testUpRight() {
	//White pawns can't move diagonally
	b->placePiece(pW, b->getSquare(1,1));
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(1,1), b->getSquare(0,2)), invalid_move_error);
	b->getSquare(1,1)->removePiece(); 

	//Black pawns can't move diagonally
	b->placePiece(pB, b->getSquare(1,1));
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(1,1), b->getSquare(0,2)), invalid_move_error);
	b->getSquare(1,1)->removePiece(); 
}

void TestPawn::testDownLeft() {
	//White pawns can't move diagonally
	b->placePiece(pW, b->getSquare(1,1));
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(1,1), b->getSquare(2,0)), invalid_move_error);
	b->getSquare(1,1)->removePiece(); 

	//Black pawns can't move diagonally
	b->placePiece(pB, b->getSquare(1,1));
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(1,1), b->getSquare(2,0)), invalid_move_error);
	b->getSquare(1,1)->removePiece(); 
}

void TestPawn::testDownRight() {
	//White pawns can't move diagonally
	b->placePiece(pW, b->getSquare(1,1));
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(1,1), b->getSquare(2,2)), invalid_move_error);
	b->getSquare(1,1)->removePiece(); 

	//Black pawns can't move diagonally
	b->placePiece(pB, b->getSquare(1,1));
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(1,1), b->getSquare(2,2)), invalid_move_error);
	b->getSquare(1,1)->removePiece(); 
}


void TestPawn::testJump() {

}

void TestPawn::testIsAlive() {
	CPPUNIT_ASSERT_EQUAL(true, pB->isAlive());
	CPPUNIT_ASSERT_EQUAL(true, pW->isAlive());
}

void TestPawn::testKill() {
	pW->kill(); 
	CPPUNIT_ASSERT_EQUAL(false, pW->isAlive());
	pB->kill(); 
	CPPUNIT_ASSERT_EQUAL(false, pW->isAlive());
}

void TestPawn::testColour() {
	///Tested by constructor
}
