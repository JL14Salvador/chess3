#include "TestRook.h" 
#include "../RookMovement.h"
#include "../Exceptions.h"

//Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION( TestRook );

void TestRook::setUp() {
	b = new ChessBoard(5,5); 
	r = new Piece(Piece::white, 'r', new RookMovement); 
}

void TestRook::tearDown() {
	delete b; 
	delete r; 
}

void TestRook::testConstructor() {
	CPPUNIT_ASSERT_EQUAL(Piece::white, r->player);
	CPPUNIT_ASSERT_EQUAL('r', r->symbol);
}

void TestRook::testLeft() {
	//Rooks can move left 
	b->placePiece(r, b->getSquare(2,2));
	b->movePiece(b->getSquare(2,2), b->getSquare(2,0)); 
	CPPUNIT_ASSERT(b->getSquare(2,0)->getPiece() == r); 
	b->getSquare(2,0)->removePiece(); 	 
}

void TestRook::testRight() {
	//Rooks can move right 
	b->placePiece(r, b->getSquare(2,2));
	b->movePiece(b->getSquare(2,2), b->getSquare(2,4)); 
	CPPUNIT_ASSERT(b->getSquare(2,4)->getPiece() == r); 
	b->getSquare(2,4)->removePiece(); 	
}

void TestRook::testUp() {
	//Rooks can move up 
	b->placePiece(r, b->getSquare(2,2));
	b->movePiece(b->getSquare(2,2), b->getSquare(0,2)); 
	CPPUNIT_ASSERT(b->getSquare(0,2)->getPiece() == r); 
	b->getSquare(0,2)->removePiece(); 
}

void TestRook::testDown() {
	//Rooks can move down 
	b->placePiece(r, b->getSquare(2,2));
	b->movePiece(b->getSquare(2,2), b->getSquare(4,2)); 
	CPPUNIT_ASSERT(b->getSquare(4,2)->getPiece() == r); 
	b->getSquare(4,2)->removePiece(); 
}

void TestRook::testUpLeft() {
	//Rooks can't move up left 
	b->placePiece(r, b->getSquare(2,2)); 
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(2,2), b->getSquare(0,0)), invalid_move_error);  
	b->getSquare(2,2)->removePiece(); 
}

void TestRook::testUpRight() {
	//Rooks can't move up right
	b->placePiece(r, b->getSquare(2,2)); 
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(2,2), b->getSquare(0,4)), invalid_move_error);  
	b->getSquare(2,2)->removePiece(); 
}

void TestRook::testDownLeft() {
	//Rooks can't down left
	b->placePiece(r, b->getSquare(2,2)); 
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(2,2), b->getSquare(4,0)), invalid_move_error);  
	b->getSquare(2,2)->removePiece(); 
}

void TestRook::testDownRight() {
	//Rooks can't move down right
	b->placePiece(r, b->getSquare(2,2)); 
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(2,2), b->getSquare(4,4)), invalid_move_error);  
	b->getSquare(2,2)->removePiece(); 
}

void TestRook::testJump() {

}

void TestRook::testIsAlive() {
	CPPUNIT_ASSERT_EQUAL(true, r->isAlive());
}

void TestRook::testKill() {
	r->kill(); 
	CPPUNIT_ASSERT_EQUAL(false, r->isAlive());
}

void TestRook::testColour() {
	///Tested by constructor
}