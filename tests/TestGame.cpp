#include "TestGame.h"
#include "../Chess.h" 
#include "KingMovement.h"
#include "PawnMovement.h"
#include "RookMovement.h"
#include "BishopMovement.h"
#include "QueenMovement.h"
#include "ChessBoard.h"

#define SIZE 6

//Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION( TestGame );

void TestGame::setUp() {
	g = new Chess(); 

	board = new ChessBoard(SIZE, SIZE);

	// Create King pieces
	bKing = new Piece(Piece::black, 'K', new KingMovement);
	wKing = new Piece(Piece::white, 'k', new KingMovement);

	// Create Rook pieces
	for (int i = 0; i < 2; i++) {
		bRooks.push_back(new Piece(Piece::black, 'R', new RookMovement));
		wRooks.push_back(new Piece(Piece::white, 'r', new RookMovement));
	}

	// Create Bishop pieces
	for (int i = 0; i < 2; i++) {
		bBishops.push_back(new Piece(Piece::black, 'B', new BishopMovement));
		wBishops.push_back(new Piece(Piece::white, 'b', new BishopMovement));
	}

	// Create Queen pieces
	bQueen = new Piece(Piece::black, 'Q', new QueenMovement);
	wQueen = new Piece(Piece::white, 'q', new QueenMovement);

	// Create Pawn pieces
	for (int i = 0; i < 6; i++) {
		bPawns.push_back(new Piece(Piece::black, 'P', new PawnMovement));
		wPawns.push_back(new Piece(Piece::white, 'p', new PawnMovement));
	}

	// Black side
	board->placePiece(bKing, board->getSquare(5, 2));
	board->placePiece(bQueen, board->getSquare(5, 3));
	board->placePiece(bRooks.at(0), board->getSquare(5, 0));
	board->placePiece(bRooks.at(1), board->getSquare(5, 5));
	board->placePiece(bBishops.at(0), board->getSquare(5, 1));
	board->placePiece(bBishops.at(1), board->getSquare(5, 4));
	for (int i = 0; i < 6; i++)
		board->placePiece(bPawns.at(i), board->getSquare(4, i));

	// White side
	board->placePiece(wKing, board->getSquare(0, 2));
	board->placePiece(wQueen, board->getSquare(0, 3));
	board->placePiece(wRooks.at(0), board->getSquare(0, 0));
	board->placePiece(wRooks.at(1), board->getSquare(0, 5));
	board->placePiece(wBishops.at(0), board->getSquare(0, 1));
	board->placePiece(wBishops.at(1), board->getSquare(0, 4));
	for (int i = 0; i < 6; i++)
		board->placePiece(wPawns.at(i), board->getSquare(1, i));

	Game::turn = 0;
	Game::numPlayers = 2;

}

void TestGame::tearDown() {
	delete g; 
	delete wKing;
	delete bKing;
	delete wQueen;
	delete bQueen;

	for (int i = 0; i < 2; i++) {
		delete bRooks.at(i);
		delete wRooks.at(i);
		delete bBishops.at(i);
		delete wBishops.at(i);
	}

	for (int i = 0; i < 6; i++) {
		delete bPawns.at(i);
		delete wPawns.at(i);
	}

	delete board;
}

void TestGame::testConstructor() {
	
}


void TestGame::testSetup() {
	g->setup();
	// CPPUNIT_ASSERT(g->board->getSquare(5,2)->getPiece() == bKing);
}

void TestGame::testIsOver(){
	board->getSquare(5,2)->getPiece()->kill();
	CPPUNIT_ASSERT(g->isOver() == false); 
}

void TestGame::testGetSquare() {

}