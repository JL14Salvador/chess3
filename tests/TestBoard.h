#ifndef TESTBOARD_H
#define TESTBOARD_H

#include <cppunit/extensions/HelperMacros.h>

#include "../Piece.h"
#include "../Board.h"

class TestBoard : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE( TestBoard ); 
	CPPUNIT_TEST( testConstructor ); 
	CPPUNIT_TEST( testPlacePiece ); 	
	CPPUNIT_TEST( testMovePiece ); 
	CPPUNIT_TEST( testBounds ); 
	CPPUNIT_TEST( testGetSquare ); 
	CPPUNIT_TEST_SUITE_END();

	Piece* p1;  
	Board* b; 

public: 
	void setUp(); 
	void tearDown(); 
	
	void testConstructor();
	void testPlacePiece(); 
	void testMovePiece();
	void testBounds(); 
	void testGetSquare(); 
};

#endif
