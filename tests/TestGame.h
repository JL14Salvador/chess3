#ifndef TESTGAME_H
#define TESTGAME_H

#include <cppunit/extensions/HelperMacros.h>

#include "../Game.h" 
#include "../Board.h"

class TestGame : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE( TestGame ); 
	CPPUNIT_TEST( testConstructor ); 
	CPPUNIT_TEST( testSetup ); 
	CPPUNIT_TEST( testIsOver ); 
	CPPUNIT_TEST( testGetSquare ); 
	CPPUNIT_TEST_SUITE_END(); 
	Game* g; 
	Piece* wKing; /*!< the White King */
	Piece* bKing; /*!< the Black King */
	std::vector<Piece*> wRooks; /*!< the White Rooks */
	std::vector<Piece*> bRooks; /*!< the Black Rooks */
	std::vector<Piece*> wPawns; /*!< the White Pawns */
	std::vector<Piece*> bPawns; /*!< the Black Pawns */
	Piece* wQueen; /*!< the White Queen */
	Piece* bQueen; /*!< the Black Queen */
	std::vector<Piece*> wBishops; /*!< the White Bishops */
	std::vector<Piece*> bBishops; /*!< the Black Bishops */
	Board* board; 


public: 
	void setUp(); 
	void tearDown(); 

	void testConstructor(); 
	void testSetup();
	void testIsOver();
	void testGetSquare();

};



#endif
