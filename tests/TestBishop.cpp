#include "TestBishop.h"
#include "../BishopMovement.h" 
#include "../Exceptions.h"

//Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION( TestBishop );

void TestBishop::setUp() {
	b = new ChessBoard(5,5); 
	bi = new Piece(Piece::white, 'b', new BishopMovement);
}

void TestBishop::tearDown() {
	delete b; 
	delete bi; 
}

void TestBishop::testConstructor() {
	CPPUNIT_ASSERT_EQUAL(Piece::white, bi->player);
	CPPUNIT_ASSERT_EQUAL('b', bi->symbol);
}

void TestBishop::testLeft() {
	//Bishops can't move left 
	b->placePiece(bi, b->getSquare(2,2)); 
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(2,2), b->getSquare(2,0)), invalid_move_error);  
	b->getSquare(2,2)->removePiece();
}

void TestBishop::testRight() {
	//Bishops can't move right
	b->placePiece(bi, b->getSquare(2,2)); 
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(2,2), b->getSquare(2,4)), invalid_move_error);  
	b->getSquare(2,2)->removePiece();   
}

void TestBishop::testUp() {
	//Bishop can't move up 
	b->placePiece(bi, b->getSquare(2,2)); 
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(2,2), b->getSquare(0,2)), invalid_move_error);  
	b->getSquare(2,2)->removePiece();  
}

void TestBishop::testDown() {
	//Bishops can't move down
	b->placePiece(bi, b->getSquare(2,2)); 
	CPPUNIT_ASSERT_THROW(b->movePiece(b->getSquare(2,2), b->getSquare(4,2)), invalid_move_error);  
	b->getSquare(2,2)->removePiece();  
}

void TestBishop::testUpLeft() {
	//Bishops can move up left 
	b->placePiece(bi, b->getSquare(2,2));
	b->movePiece(b->getSquare(2,2), b->getSquare(0,0)); 
	CPPUNIT_ASSERT(b->getSquare(0,0)->getPiece() == bi); 
	b->getSquare(0,0)->removePiece(); 
}

void TestBishop::testUpRight() {
	//Bishops can move up right 
	b->placePiece(bi, b->getSquare(2,2));
	b->movePiece(b->getSquare(2,2), b->getSquare(0,4)); 
	CPPUNIT_ASSERT(b->getSquare(0,4)->getPiece() == bi); 
	b->getSquare(0,4)->removePiece(); 
}

void TestBishop::testDownLeft() {
	//Bishops can move down left 
	b->placePiece(bi, b->getSquare(2,2));
	b->movePiece(b->getSquare(2,2), b->getSquare(4,0)); 
	CPPUNIT_ASSERT(b->getSquare(4,0)->getPiece() == bi); 
	b->getSquare(4,0)->removePiece(); 
}

void TestBishop::testDownRight() {
	//Bishops can move down right
	b->placePiece(bi, b->getSquare(2,2));
	b->movePiece(b->getSquare(2,2), b->getSquare(4,4)); 
	CPPUNIT_ASSERT(b->getSquare(4,4)->getPiece() == bi); 
	b->getSquare(4,4)->removePiece(); 
}

void TestBishop::testJump() {

	// b->movePiece(b->getSquare(0,0), b->getSquare(0,1));
	// Piece* temp = b->getSquare(0,1)->getPiece(); 

}

void TestBishop::testIsAlive() {
	CPPUNIT_ASSERT_EQUAL(true, bi->isAlive());
}

void TestBishop::testKill() {
	bi->kill(); 
	CPPUNIT_ASSERT_EQUAL(false, bi->isAlive());	
}

void TestBishop::testColour() {
	///Tested by constructor
}