/*
 * KingMovement.cpp
 *
 *  Created on: Sep 17, 2015
 *      Author: anvik
 */

#include "KingMovement.h"
#include "Exceptions.h"
#include <math.h>

std::vector<const Coord*> KingMovement::path(const Coord* s, const Coord* e) {

	if( abs(s->x - e->x) > 1 || abs(s->y - e->y) > 1 ) {
		delete s; 
		delete e; 
		throw invalid_move_error ("Kings can only move 1 space.");
	}
	/*double distance = sqrt((abs(s->x - e->x) + abs(s->y - e->y)));
	if (distance > 1)*/
	std::vector<const Coord*> path;
	path.push_back(s);
	path.push_back(e);
	// delete s; 
	// delete e; 
	return path;
}
