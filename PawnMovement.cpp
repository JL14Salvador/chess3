/*
 * PawnMovement.cpp
 *
 *  Created on: Sep 17, 2015
 *      Author: anvik
 */

#include "PawnMovement.h"
#include "Exceptions.h"
#include <math.h>
#include "Game.h"

std::vector<const Coord*> PawnMovement::path(const Coord* s, const Coord* e) {
	if (s->y != e->y){
		delete s; 
		delete e; 
		throw invalid_move_error("Pawns move vertically");		
	}

	if (e->x - s->x < 0  && Game::turn % 2 != 1) {
		delete s; 
		delete e; 
		throw invalid_move_error("White's pawns can't move backwards");
	}

	if (e->x - s->x > 0 && Game::turn % 2 != 0) {
		delete s; 
		delete e; 
		throw invalid_move_error("Black's pawns can't move backwards");
	}

	if( abs(s->x - e->x) > 1 || abs(s->y - e->y) > 1 ){
		delete s; 
		delete e; 
		throw invalid_move_error("Pawns move only 1 space");
	}

	std::vector<const Coord*> p;
	p.push_back(s);
	p.push_back(e);
	// delete s; 
	// delete e; 
	return p;
}

